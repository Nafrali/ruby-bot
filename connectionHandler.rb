#connectionHandler.rb

require 'socket'
require_relative 'admin'


class CHandle
	@admins
	@socket

	def initialize (socket)
		@socket = socket
		@admins = Admin.new
		@admins.addAdmin ("BNafrali")
	end


	def recieve(message)
		if message.start_with? 'PING'
			message[1] = 'O'
			send(message)
			return
		end

		fmessage = message.split(' ')
		if fmessage[1] == 'PRIVMSG'
			privmsg(message)
		elsif fmessage[1] == 'JOIN'
			join(fmessage)
			serverMessage(message)
		else
			serverMessage(message)
		end
	end

	def send(message)
		@socket.puts(message)
	end

	def serverMessage (message)
		puts message
	end

	def getUsername(message)
		exl = message.index("!")
		user = message.slice(1, exl-1)
		return user		
	end

	def join(message)
			puts "#{message[0]} joined #{message[2]}"
			send("PRIVMSG #{message[2]} :Hi! We're a small bunch of guys just sitting here, idling.")
	end

	def privmsg (message)
		fmessage = message.split(' ')
		if fmessage[1] == 'PRIVMSG'
			user = getUsername(message)
			msg = fmessage.slice(3, fmessage.size)
			msg = msg.join(" ")
			msg.slice!(0)
			puts "#{user}: #{msg}"
			channel = fmessage[2]
			
			if (@admins.isAdmin(user) && channel == "Nafrali" && msg == "Join")
				send("JOIN #gaymen\r\n")
				return
			end
		
			if (@admins.isAdmin(user) && channel == "Nafrali")
				send("#{msg}")
				return
			end

		end
	end
end

