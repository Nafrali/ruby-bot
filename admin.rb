class Admin
	@list

	def initialize
		@list = []
	end

	def isAdmin (name)
		return @list.include? (name)
	end

	def addAdmin (name)
		if !(isAdmin(name))
			@list.push(name)
		end
	end

	def removeAdmin (name)
		if (isAdmin (name))
			@list.delete(name)
		end
	end
end
