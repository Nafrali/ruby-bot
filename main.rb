require_relative 'connect'

def readServerList()
	list = Hash.new

	file = File.open('servers.list')
	doc = Nokogiri::XML(file)
	file.close

	doc.xpath('//server').each do | s|
		ip = s.at_xpath('ip').content
		port = s.at_xpath('port').content
		if (ip != nil && port != nil)
			list[ip] = port
		end
	end

	return list
end


@threadList = []
sList = readServerList()
sList.each { |key,value|
	puts key + " " + value
	server = IrcServer.new key, value
	t1 = Thread.new{server.connect}
	@threadList.push(t1)
	puts t1
}
puts @threadList.size
@threadList.each do |thread|
	thread.join
end
#threadList[0].join

